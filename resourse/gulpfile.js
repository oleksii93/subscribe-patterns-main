var gulp = require('gulp'),
    sass = require('gulp-sass'),
    concat = require('gulp-concat'),
    rename = require('gulp-rename'),
    uglify = require('gulp-uglify-es').default;


gulp.task('sass-front', function() {
    gulp.src('app/scss/front.scss')
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(concat('main.css'))
        .pipe(gulp.dest('../assets/css'))
        .pipe(rename('main.min.css'))
        .pipe(gulp.dest('../assets/css'));

});

gulp.task('scripts-front', function() {
  gulp.src('app/js/front/*.js')
    .pipe(concat('custom-front.js'))
    .pipe(gulp.dest('../assets/js'))
    .pipe(rename('custom-front.min.js'))
    .pipe(uglify())
    .pipe(gulp.dest('../assets/js'));
});

gulp.task('default', function() {
    gulp.run("sass-front");
    gulp.run("scripts-front");
    gulp.watch('app/scss/**/*.scss', function() {
        gulp.run('sass-front');
    });
    gulp.watch('app/js/front/*.js', function() {
        gulp.run('scripts-front');
    });
});
