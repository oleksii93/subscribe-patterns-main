# Overview
Test task Oleksii Panchenko

# Requirements
Make sure all dependencies have been installed before moving on:

**Development machine**
- NPM
- Gulp

**WordPress**
- WordPress >= 4.9
- PHP >= 7.2.0

# Plugin development
- `npm i` — Install Gulp, and it's packages using NPM in the plugin directory.

## Build commands
- `gulp` — Compiles continuously SASS and JS
