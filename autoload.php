<?php

/**
 * Autoloader
 *
 * @package SPM
 * @since 1.0
 */

spl_autoload_register( function ( $classname ) {
    if ( false !== strpos( $classname, 'SPM' ) ) {
        $classFile = str_replace( '\\', DIRECTORY_SEPARATOR, $classname );
        require_once(SUBSCRIBE_PATH . DIRECTORY_SEPARATOR . "{$classFile}.php");
    }
} );