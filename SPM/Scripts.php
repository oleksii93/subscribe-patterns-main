<?php
/**
 * Class Scripts
 * @package SPM
 * @since 1.0
 */

namespace SPM;

class Scripts
{

    /**
     * Scripts version.
     *
     * @var string
     */
    public $version = '1.0';

    /**
     * The single instance of the class
     * @var Scripts
     */
    protected static $instance = null;

    /**
     * @return \SPM\Scripts
     */
    public static function get_instance()
    {
        if ( is_null( self::$instance ) ) {
            self::$instance = new Scripts();
        }

        return self::$instance;
    }

    /**
     * Scripts constructor.
     */
    private function __construct()
    {
        add_action( 'wp_enqueue_scripts', [ $this, 'register_styles' ] );
        add_action( 'wp_enqueue_scripts', [ $this, 'register_scripts' ] );
    }

    /**
     * Register styles
     */
    public function register_styles()
    {
        wp_register_style(
            'subscribe',
            SUBSCRIBE_URL . '/assets/css/main.min.css',
            [],
            SUBSCRIBE_VERSION
        );
    }

    /**
     * Register scripts
     */
    public function register_scripts()
    {
        wp_register_script(
            'subscribe',
            SUBSCRIBE_URL . '/assets/js/custom-front.min.js',
            [],
            SUBSCRIBE_VERSION,
            true
        );

        wp_localize_script(
            'subscribe',
            'subscribe',
            [
                'adminUrl' => admin_url( 'admin-ajax.php' ),
                'nonce'    => wp_create_nonce( Subscribe::SUBSCRIBE_NONCE_ACTION ),
            ]
        );
    }

}