<?php
/**
 * Class Shortcode
 * @package SPM
 * @since 1.0
 */

namespace SPM;

class Shortcode
{

    /**
     * Shortcode version.
     *
     * @var string
     */
    public $version = '1.0';

    /**
     * The single instance of the class
     * @var Shortcode
     */
    protected static $instance = null;

    /**
     * @return \SPM\Shortcode
     */
    public static function get_instance()
    {
        if ( is_null( self::$instance ) ) {
            self::$instance = new Shortcode();
        }

        return self::$instance;
    }

    /**
     * Shortcode constructor.
     */
    private function __construct()
    {
    }

    /**
     * View form
     *
     * @return string
     */
    public function form()
    {
        wp_enqueue_style( 'subscribe' );
        wp_enqueue_script( 'subscribe' );

        ob_start();

        include_once(SUBSCRIBE_PATH . '/templates/form.php');

        return ob_get_clean();
    }

}