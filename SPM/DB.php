<?php
/**
 * Class DB
 * @package SPM
 * @since 1.0
 */

namespace SPM;

class DB
{
    /**
     * DB version.
     *
     * @var string
     */
    public $version = '1.0';

    /**
     * The single instance of the class
     * @var DB
     */
    protected static $instance = null;

    /**
     * @return \SPM\DB
     */
    public static function get_instance()
    {
        if ( is_null( self::$instance ) ) {
            self::$instance = new DB();
        }

        return self::$instance;
    }

    /**
     * DB constructor.
     */
    public function __construct()
    {
    }

    /**
     * Save subscriber
     *
     * @param $email
     *
     * @return false|int
     */
    public function save_subscriber( $email )
    {
        global $wpdb;

        return $wpdb->replace(
            $this->get_table_name(),
            [
                'email' => sanitize_email( $email ),
            ],
            [
                'email' => '%s',
            ]
        );
    }

    /**
     * Get table name
     *
     * @return string
     */
    private function get_table_name()
    {
        global $wpdb;

        return $wpdb->prefix . 'subscribers';
    }

    /**
     * Create table
     */
    public function create_table()
    {
        require_once ABSPATH . 'wp-admin/includes/upgrade.php';

        global $wpdb;

        $table_name = $this->get_table_name();

        $sql = "CREATE TABLE {$table_name} (
			email VARCHAR(255) UNIQUE NOT NULL,
			PRIMARY KEY (email)
		) {$wpdb->get_charset_collate()};";

        dbDelta( $sql );
    }

}