<?php
/**
 * Class Subscribe
 * @package SPM
 * @since 1.0
 */

namespace SPM;

class Subscribe
{

    /**
     * Subscribe version.
     *
     * @var string
     */
    public $version = '1.0';

    /**
     * The single instance of the class
     * @var Subscribe
     */
    protected static $instance = null;

    /**
     * @return \SPM\Subscribe
     */
    public static function get_instance()
    {
        if ( is_null( self::$instance ) ) {
            self::$instance = new Subscribe();
        }

        return self::$instance;
    }

    const SUBSCRIBE_NONCE_ACTION = 'subscribe-action';

    /**
     * Subscribe constructor.
     */
    private function __construct()
    {
        //Init classes
        $this->setup_environment();

        // Action
        add_action( 'wp_ajax_subscribe', [ $this, 'subscribe' ] );
        add_action( 'wp_ajax_nopriv_subscribe', [ $this, 'subscribe' ] );

        // Shortcode
        add_shortcode( 'subscribe_form', [ 'SPM\Shortcode', 'form' ] );
    }

    /**
     * Init class
     */
    public function setup_environment()
    {
        Scripts::get_instance();
    }

    /**
     * Subscribe
     */
    public function subscribe()
    {
        check_ajax_referer( self::SUBSCRIBE_NONCE_ACTION );

        $email = filter_input( INPUT_POST, 'email', FILTER_SANITIZE_EMAIL );

        if ( ! is_email( $email ) ) {
            wp_send_json_error(
                sprintf(
                    esc_html__( 'The %s is invalid email', 'subscribe' ),
                    esc_html( $email )
                )
            );
        }

        if ( 2 === (new DB)->save_subscriber( $email ) ) {
            wp_send_json_error(
                sprintf(
                    esc_html__( 'The %s email is already exists', 'subscribe' ),
                    esc_html( $email )
                )
            );
        }

        wp_send_json_success( esc_html__( 'You were successfully subscribed', 'subscribe' ) );
    }

}
